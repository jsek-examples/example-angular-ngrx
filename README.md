# Example Angular + Ngrx

Exmple Angular 6.1.0 app with Ngrx 6.1.0

<!-- ![](./screenshot.png) -->

## Setup

```sh
npm i
npm start # serve
```

## Scaffolding

```sh
npm run ng -- generate component component-name 
# or directive | pipe | service | module | etc.
```
