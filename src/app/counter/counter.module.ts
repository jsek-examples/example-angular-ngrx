import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { counterReducer } from './counter';
import { StoreModule } from '@ngrx/store';
import { CounterComponent } from './counter.component';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forRoot({ count: counterReducer }),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      // https://github.com/ngrx/platform/blob/master/docs/store-devtools/README.md#instrumentation-options
    }),
  ],
  declarations: [CounterComponent],
  exports: [CounterComponent]
})
export class CounterModule { }
